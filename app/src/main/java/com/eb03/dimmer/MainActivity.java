package com.eb03.dimmer;

import android.Manifest;
import android.bluetooth.BluetoothAdapter;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import com.eb03.roundslider.RoundSlider;

import java.util.UUID;

import static android.os.Build.VERSION.SDK_INT;

public class MainActivity extends AppCompatActivity {

    private final static int BT_CONNECT_CODE = 1;
    private final static int PERMISSIONS_REQUEST_CODE = 0;
    private final static String[] BT_DANGEROUS_PERMISSIONS = new String[]{Manifest.permission.ACCESS_FINE_LOCATION};
    private TextView mStatus;
    private RoundSlider mRoundSlider;
    private int previousValueSlider;

    private OscilloManager mOscillo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mStatus = findViewById(R.id.status);
        mRoundSlider = findViewById(R.id.RoundSlider);
        mRoundSlider.setRoundSliderChangeListener(new RoundSlider.RoundSliderChangeListener() {
            @Override
            public void onChange(float value) {
                if(previousValueSlider != value){
                    byte[] cmd = new byte[1];
                    cmd[0] = mOscillo.setCalibrationDutyCycle(value);
                    mOscillo.sendCmd(cmd);
                }
                previousValueSlider = Math.round(value);
            }

            @Override
            public void onDoubleClick(float value) {
                byte[] cmd = new byte[1];
                cmd[0] = mOscillo.setCalibrationDutyCycle(0);
                mOscillo.sendCmd(cmd);
            }
        });
        verifyBtRights();
    }

    @Override
    protected void onStart() {
        super.onStart();
        mOscillo = OscilloManager.getInstance();
        mOscillo.setOscilloListener(new OscilloManager.OscilloEventsListener(){

            @Override
            public void onDeviceDataSend(byte[] data) {

            }

            @Override
            public void onDeviceStateChanged(int state) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        switch (state){
                            case Transceiver.STATE_NOT_CONNECTED:
                                Toast.makeText(MainActivity.this, "Not connected",Toast.LENGTH_LONG).show();
                                mStatus.setText("Not connected");
                                mRoundSlider.setEnabled(false);
                                break;

                            case Transceiver.STATE_CONNECTING:
                                Toast.makeText(MainActivity.this, "Connecting",Toast.LENGTH_LONG).show();
                                mStatus.setText("Connecting to ");
                                break;

                            case Transceiver.STATE_CONNECTED:
                                Toast.makeText(MainActivity.this, "Connected",Toast.LENGTH_LONG).show();
                                mRoundSlider.setEnabled(true);
                                break;

                        }
                    }
                });
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        int menuItem = item.getItemId();
        switch (menuItem) {
            case R.id.connect:
                Intent BTConnect;
                BTConnect = new Intent(this, BTConnectActivity.class);
                startActivityForResult(BTConnect, BT_CONNECT_CODE);
                break;
        }
        return true;
    }


    private void verifyBtRights() {
        if (BluetoothAdapter.getDefaultAdapter() == null) {
            Toast.makeText(this, "Cette application nécessite un adaptateur BT", Toast.LENGTH_LONG).show();
            finish();
            return;
        }

        if (SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_DENIED) {
                requestPermissions(BT_DANGEROUS_PERMISSIONS, PERMISSIONS_REQUEST_CODE);
            }
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == PERMISSIONS_REQUEST_CODE) {
            if (grantResults[0] == PackageManager.PERMISSION_DENIED) {
                Toast.makeText(this, "Les autorisations BT sont requises pour utiliser l'application", Toast.LENGTH_LONG).show();
                finish();
                return;
            }

        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case BT_CONNECT_CODE:
                if (resultCode == RESULT_OK) {
                    String address = data.getStringExtra("device");
                    mStatus.setText(address);
                }
                break;
            default:
        }
    }
}



