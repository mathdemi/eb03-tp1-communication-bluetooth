package com.eb03.dimmer;

public class OscilloManager {


    private static OscilloManager INSTANCE;
    private Transceiver mOscillo;
    private FrameProcessor mFrameProcessor;
    private OscilloEventsListener mOscilloListener;
    private Transceiver.TransceiverListener mTransceiverListener;


    private OscilloManager(){

        mTransceiverListener = new Transceiver.TransceiverListener() {


            @Override
            public void onTransceiverDataSent(byte[] data) {
                mOscilloListener.onDeviceDataSend(data);
            }

            @Override
            public void onTransceiverStateChanged(int state) {
                mOscilloListener.onDeviceStateChanged(state);
            }

            @Override
            public void onTransceiverConnectionlost() {

            }

            @Override
            public void onTransceiverUnableToConnect() {

            }
        };

    }

    public static OscilloManager getInstance(){
        if (INSTANCE == null){
            INSTANCE = new OscilloManager();
        }
        return INSTANCE;
    }

    public void attachOscillo(Transceiver Oscillo){
        mOscillo = Oscillo;
        mOscillo.setTransceiverListener(mTransceiverListener);
        mOscillo.attachFrameProcessor(new FrameProcessor());
    }

    public void sendCmd(byte[] cmd){
        mOscillo.send(cmd);
    }

    public void connectDevice(String id){
        mOscillo.connect(id);
    }

    public byte setCalibrationDutyCycle(float alpha){
//        return (int) (alpha * 100);
          return (byte) Math.round(alpha);
    }

    public void setOscilloListener(OscilloEventsListener oscilloListener){
        mOscilloListener = oscilloListener;
    }

    /*********************************************************************************************
     *
     *                                         INTERFACES
     *
     ********************************************************************************************/

    public interface OscilloEventsListener{
        void onDeviceDataSend(byte[] data);
        void onDeviceStateChanged(int state);
    }
}