package com.eb03.dimmer;

public class ByteRingBuffer {

    private int mBufferSize;
    private int mReadIndex;
    private int mWriteIndex;
    private int mUsedSize;
    private byte[] mBuffer;


    /**
     * Constructeur du buffer cicrulaire permettant d'initialiser tous ses attributs
     *
     * @param size taille du buffer à créer
     */
    public ByteRingBuffer(int size) {
        mBuffer = new byte[size];
        mBufferSize = size;
        mReadIndex = 0;
        mWriteIndex = 0;
        mUsedSize = 0;
    }

    /**
     * Methode permettant d'écrire un octet dans le buffer circulaire
     *
     * @param msg octet à écrire
     * @return état de la réussite de l'écriture
     */
    public boolean WriteBuffer(byte msg){
        if(isOverflowed()) return false;
        mBuffer[mWriteIndex++] = msg;
        mUsedSize ++;
        if(mWriteIndex == mBuffer.length) mWriteIndex = 0;
        return true;
    }

    /**
     * Methode permettant d'écrire un tableau octet dans le buffer circulaire
     *
     * @param msg tableau d'octet à écrire dans le buffer
     * @return état de la réussite de l'écriture
     */
    public boolean WriteBuffer(byte[] msg){
        for(int i = 0; i < msg.length;i++){
            if(!WriteBuffer(msg[i])) return false;
        }
        return true;
    }


    /**
     * @return le premier caractère à lire
     */
    public byte ReadBuffer(){
        if(isEmpty()) return 0;

        byte byteToRead = mBuffer[mReadIndex++];
        mUsedSize --;
        if(mReadIndex == mBuffer.length) mReadIndex = 0;

        return byteToRead;
    }


    /**
     * @return 1 si le buffer est vide
     */
    public boolean isEmpty(){return mUsedSize == 0;}

    /**
     * @return 1 si le buffer est plein
     */
    public boolean isOverflowed(){
        return mUsedSize == mBuffer.length;
    }

}
