package com.eb03.dimmer;

import java.io.ByteArrayOutputStream;

public class FrameProcessor {

    final static byte HEADER = 0x05;
    final static byte TAIL = 0x04;
    final static byte ESC = 0x06;
    final static byte setCalibrationDutyCycle = 0xA;


    /**
     * Méthode permettant de créer la frame correspondante à l'envoi d'une valeur de rapport cyclique indiqué en paramètre
     *
     * @param value : valeur du paramètre de la commande appelé, en l'occurence la valeur du rapport cyclique souhaité
     * @return frame sous forme de tableau d'octet à envoyer à la cible
     */
    public byte[] toFrame(byte[] value) {
        ByteArrayOutputStream frame = new ByteArrayOutputStream();
        int ctrl = 0;

        // HEADER
        frame.write(HEADER);

        // LENGTH
        byte[] length = new byte[2];
        if((value.length + 1) > 0xFF)
            length[1] = (byte) ((value.length + 1) - 0xFF);
        length[0] = (byte) ((value.length + 1) % 0xFF);
        writeToFrame(frame,length[1]);
        writeToFrame(frame,length[0]);

        ctrl += (value.length + 1) ;

        // ID
        writeToFrame(frame, setCalibrationDutyCycle);    // ID pour changer le rapport cyclique de pilotage de la LED
        ctrl += setCalibrationDutyCycle;

        // PAYLOAD
        for (int i = 0; i < value.length; i++) {
            writeToFrame(frame,value[i]);
            ctrl += value[i];
        }
        //CTRL
        ctrl = ~ctrl;
        ctrl += 1;
        ctrl %= 256;
        writeToFrame(frame,(byte) ctrl);

        // TAIL
        frame.write(TAIL);
        return frame.toByteArray();
    }

    /**
     * Méthode permettant de vérifier que le caractère à écrire n'est pas un caractère reservé, si tel est le cas alors le caractère d'échappement est inscrit dans la frame suivi de la valeur à écrire additionnée à la valeur du caractère d'échappement
     *
     * @param frame tableau d'octet contenant la frame en construction
     * @param data valeur du caractère à écrire dans la frame
     */
    private void writeToFrame(ByteArrayOutputStream frame, byte data) {
        if((data == HEADER) || (data == TAIL) || (data == ESC)){
            frame.write(ESC);
            frame.write(data + ESC);
        }
        else
            frame.write(data);
    }
}
