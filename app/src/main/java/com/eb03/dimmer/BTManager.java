package com.eb03.dimmer;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.util.Log;

import java.io.IOException;
import java.io.OutputStream;
import java.util.UUID;

public class BTManager extends Transceiver {

    private static final UUID MY_UUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");

    private BluetoothAdapter mAdapter;

    private BluetoothSocket mSocket = null;

    private ConnectThread mConnectThread = null;
    private WritingThread mWritingThread = null;

    public BTManager(BluetoothAdapter bluetoothAdapter) {
        this.mAdapter = bluetoothAdapter;
    }


    @Override
    public void connect(String id) {
        BluetoothDevice device = BluetoothAdapter.getDefaultAdapter().getRemoteDevice(id);
        disconnect();
        mConnectThread = new ConnectThread(device);
        setState(STATE_CONNECTING);
        mConnectThread.start();
    }


    @Override
    public void disconnect() {
        if(mConnectThread != null){
            mConnectThread.close();
            mConnectThread = null;
        }
        setState(STATE_NOT_CONNECTED);
    }


    @Override
    public void send(byte[] b) {
        byte [] frame = mFrameProcessor.toFrame(b);
        mWritingThread.write(frame);
        mTransceiverListener.onTransceiverDataSent(frame);
    }


    private void startReadWriteThreads() {
        // instanciation d'un thread de lecture

        mWritingThread = new WritingThread(mSocket);
        mWritingThread.start();
        setState(STATE_CONNECTED);
    }

    /**
     * Classe du Thread de connexion au device
     */
    private class ConnectThread extends Thread {


        public ConnectThread(BluetoothDevice device) {
            try {
                mSocket = device.createRfcommSocketToServiceRecord(MY_UUID);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        @Override
        public void run() {
            mAdapter.cancelDiscovery();

            try {
                mSocket.connect();
            } catch (IOException e) {
                close();
                mTransceiverListener.onTransceiverUnableToConnect();
                setState(STATE_NOT_CONNECTED);
            }
            if(mSocket != null)
                startReadWriteThreads();
        }

        public void close() {
            try {
                if(mWritingThread != null){
                    mSocket.getOutputStream().close();
                    mWritingThread = null;
                }

                if(mSocket != null) {
                    mSocket.close();
                    mSocket = null;
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Classe du Thread d'écriture vers le device
     */
    private class WritingThread extends Thread {
        private OutputStream mOutStream;
        private ByteRingBuffer ringBuffer;

        public WritingThread(BluetoothSocket mSocket) {
            ringBuffer = new ByteRingBuffer(2048);
            try {
                mOutStream = mSocket.getOutputStream();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        @Override
        public void run() {
            while (mSocket != null) { {
                    try {
                        if(!ringBuffer.isEmpty())
                            mOutStream.write(ringBuffer.ReadBuffer());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }

        public void write(byte[] msg){
            ringBuffer.WriteBuffer(msg);
        }

    }
}

